<?php

namespace Drupal\filter_format_audit\EntityHandlers;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a default filter format audit handler class.
 */
class FilterFormatAuditHandlerDefault implements FilterFormatAuditHandlerInterface, EntityHandlerInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new FilterFormatAuditHandlerDefault.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl(ContentEntityInterface $entity): ?Url {
    return $entity->access('view') ? $entity->toUrl() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(ContentEntityInterface $entity) : ?string {
    return $entity->access('view label') ? $entity->label() : new TranslatableMarkup('Redacted [@entity_type:@entity_id]', [
      '@entity_type' => $entity->getEntityTypeId(),
      '@entity_id' => $entity->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getEditUrl(ContentEntityInterface $entity) : ?Url {
    return $entity->access('update') ? $entity->toUrl('edit-form') : NULL;
  }

}
