<?php

namespace Drupal\filter_format_audit\EntityHandlers;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines a filter format audit handler for paragraph entities.
 */
class ParagraphFilterFormatAuditHandler extends FilterFormatAuditHandlerDefault {

  use EntityWithHostFilterFormatAuditHandlerTrait;

  /**
   * {@inheritdoc}
   */
  protected function getHostEntity(ContentEntityInterface $entity): ?ContentEntityInterface {
    /** @var \Drupal\paragraphs\ParagraphInterface $entity */
    return $entity->getParentEntity();
  }

  /**
   * {@inheritdoc}
   */
  protected function hasHostEntity(ContentEntityInterface $entity): bool {
    return TRUE;
  }

}
