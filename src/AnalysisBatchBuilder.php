<?php

declare(strict_types=1);

namespace Drupal\filter_format_audit;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\filter\FilterFormatInterface;
use Drupal\filter_format_audit\Form\RunAnalysisForm;
use Drupal\text\Plugin\Field\FieldType\TextItemBase;

/**
 * Defines a class for building the analysis batch task.
 */
final class AnalysisBatchBuilder {

  /**
   * Builds a batch task for analysis.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity field manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $fieldTypePluginManager
   *   Field type plugin manager.
   *
   * @return array
   *   Batch jobs
   */
  public static function buildAnalysisBatch(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager, FieldTypePluginManagerInterface $fieldTypePluginManager): array {
    $ids = $entityTypeManager->getStorage('analysis_result')
      ->getQuery()
      ->accessCheck(FALSE)
      ->execute();
    $batch = [
      'title' => new TranslatableMarkup('Analysing content ...'),
      'operations' => [],
      'finished' => [RunAnalysisForm::class, 'finished'],
    ];
    foreach (array_chunk($ids, 50) as $chunk) {
      $batch['operations'][] = [[RunAnalysisForm::class, 'clear'], [$chunk]];
    }
    $formats = array_filter($entityTypeManager->getStorage('filter_format')->loadMultiple(), function (FilterFormatInterface $format) {
      return $format->filters('filter_html') && $format->filters('filter_html')->status;
    });
    foreach ($entityFieldManager->getFieldMap() as $entity_type_id => $fields) {
      if ($entity_type_id === 'analysis_result') {
        // Inception.
        continue;
      }
      $entity_type_storage = $entityTypeManager->getStorage($entity_type_id);
      foreach ($fields as $field_name => $details) {
        $type = $details['type'];
        $plugin = $fieldTypePluginManager->getDefinition($type, FALSE);
        if (!$plugin || !is_a($plugin['class'], TextItemBase::class, TRUE)) {
          continue;
        }
        try {
          $query = $entity_type_storage->getQuery()
            ->exists($field_name)
            ->accessCheck(FALSE)
            ->condition("$field_name.format", array_keys($formats), 'IN');
          $results = $query->execute();
        }
        catch (QueryException $e) {
          // Corrupt field map.
          continue;
        }
        if ($results) {
          foreach ($results as $entity_id) {
            $batch['operations'][] = [
              [RunAnalysisForm::class, 'analyse'],
              [$entity_type_id, $entity_id, $field_name],
            ];
          }
        }
      }

    }
    return $batch;
  }

}
