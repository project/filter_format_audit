<?php

declare(strict_types=1);

namespace Drupal\filter_format_audit\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Defines a views field plugin for the field name in an analysis result.
 *
 * @ViewsField("filter_format_audit_field")
 */
final class FieldName extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    $content_entity = $entity->get('content')->entity;
    $field_name = $entity->field_name->value;
    if (!$content_entity) {
      return $field_name;
    }
    $field_definition = $content_entity->getFieldDefinition($field_name);
    if ($field_definition) {
      return $field_definition->getLabel();
    }
    return $field_name;
  }

}
