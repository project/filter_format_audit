<?php

namespace Drupal\filter_format_audit\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\filter_format_audit\AnalysisBatchBuilder;
use Drupal\filter_format_audit\EntityHandlers\FilterFormatAuditHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for rebuilding the analysis.
 */
class RunAnalysisForm extends FormBase {

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->fieldTypePluginManager = $container->get('plugin.manager.field.field_type');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'filter_format_audit_run_analysis';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return [
      'warning' => ['#markup' => '<p><em>This will delete all existing analysis results</em></p>'],
      'submit' => ['#type' => 'submit', '#value' => $this->t('Run analysis')],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entityTypeManager = $this->entityTypeManager;
    $entityFieldManager = $this->entityFieldManager;
    $fieldTypePluginManager = $this->fieldTypePluginManager;
    $batch = AnalysisBatchBuilder::buildAnalysisBatch($entityTypeManager, $entityFieldManager, $fieldTypePluginManager);
    batch_set($batch);
    $form_state->setRedirect('entity.analysis_result.collection');
  }

  /**
   * Batch callback.
   */
  public static function clear($ids, array|\DrushBatchContext &$context) {
    $storage = \Drupal::entityTypeManager()->getStorage('analysis_result');
    $storage->delete($storage->loadMultiple($ids));
    $context['message'] = t('Deleting existing results');
  }

  /**
   * Batch callback.
   */
  public static function analyse(string $entity_type_id, $entity_id, string $field_name, array|\DrushBatchContext &$context) {
    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_type_storage = $entity_type_manager->getStorage($entity_type_id);
    /** @var \Drupal\filter_format_audit\EntityHandlers\FilterFormatAuditHandlerInterface $handler */
    $handler = $entity_type_manager->getHandler($entity_type_id, FilterFormatAuditHandlerInterface::HANDLER_TYPE);
    $entity_type = $entity_type_manager->getDefinition($entity_type_id);
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $entity_type_storage->load($entity_id);
    if (!$entity) {
      return;
    }
    $field_definition = $entity->getFieldDefinition($field_name);
    $context['message'] = t('Now processing @type %label (@id), %field_name field', [
      '@type' => $entity_type->getLabel(),
      '@id' => $entity_id,
      '%label' => $handler->getLabel($entity),
      '%field_name' => $field_definition ? $field_definition->getLabel() : t('Unknown'),
    ]);
    $result = \Drupal::service('filter_format_audit.content_analysis')->analyse($entity, $field_name);
    if ($result->hasIssues()) {
      $result->save();
    }
    $context['results'][] = sprintf('%s:%s:%s', $entity_type_id, $entity_id, $field_name);
  }

  /**
   * Batch finished callback.
   */
  public static function finished(bool $success, array $results, array $operations, string $elapsed) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(new PluralTranslatableMarkup(count($results), 'Analysed 1 item in @time.', 'Analysed @count items in @time.', [
        '@time' => $elapsed,
      ]));
      return;
    }
    $messenger->addError(new PluralTranslatableMarkup(count($operations), 'An error occurred during processing, 1 item remains unprocessed.', 'An error occurred during processing, @count items remains unprocessed.'));
  }

}
