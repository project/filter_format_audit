<?php

declare(strict_types=1);

namespace Drupal\filter_format_audit\Plugin\views\field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\filter_format_audit\EntityHandlers\FilterFormatAuditHandlerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a views field plugin for the content entity reference.
 *
 * @ViewsField("filter_format_audit_content")
 */
final class ContentEntityReference extends FieldPluginBase {

  /**
   * Entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    $content_entity = $entity->get('content')->entity;
    if (!$content_entity) {
      return $this->t('Deleted');
    }
    $handler = $this->entityTypeManager->getHandler($content_entity->getEntityTypeId(), FilterFormatAuditHandlerInterface::HANDLER_TYPE);
    assert($handler instanceof FilterFormatAuditHandlerInterface);
    $url = $handler->getUrl($content_entity);
    $label = $handler->getLabel($content_entity);
    if ($url) {
      return [
        '#type' => 'link',
        '#url' => $url,
        '#title' => $label,
      ];
    }
    return $label;
  }

}
