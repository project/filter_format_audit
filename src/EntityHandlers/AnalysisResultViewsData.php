<?php

declare(strict_types=1);

namespace Drupal\filter_format_audit\EntityHandlers;

use Drupal\views\EntityViewsData;

/**
 * Defines a class for providing views data for analysis result entities.
 */
final class AnalysisResultViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();
    $base_table = $this->entityType->getBaseTable() ?: $this->entityType->id();
    $data[$base_table]['field_name']['field']['id'] = 'filter_format_audit_field';
    $data[$base_table]['content__target_id']['field']['id'] = 'filter_format_audit_content';
    return $data;
  }

}
