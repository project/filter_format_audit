<?php

namespace Drupal\filter_format_audit;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\filter_format_audit\EntityHandlers\FilterFormatAuditHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a list builder for analysis results.
 */
class AnalysisResultListBuilder extends EntityListBuilder {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'content' => $this->t('Content'),
      'field_name' => $this->t('Field name'),
      'format' => $this->t('Filter format'),
      'stripped_tags' => $this->t('Stripped tags'),
      'stripped_attributes' => $this->t('Stripped attributes'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);
    /** @var \Drupal\filter_format_audit\EntityHandlers\FilterFormatAuditHandlerInterface $handler */
    $content_entity = $entity->content->entity;
    if (!$content_entity) {
      return $operations;
    }
    $handler = $this->entityTypeManager->getHandler($content_entity->getEntityTypeId(), FilterFormatAuditHandlerInterface::HANDLER_TYPE);
    if ($url = $handler->getEditUrl($content_entity)) {
      $operations['edit'] = [
        'title' => $this->t('Edit content'),
        'weight' => -50,
        'url' => $this->ensureDestination($url),
      ];
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $content_entity = $entity->content->entity;
    $stripped = [
      'filter_format' => [
        'data' => [
          '#theme' => 'item_list',
          '#items' => array_column($entity->get('text')->getValue(), 'format'),
        ],
      ],
      'stripped_tags' => [
        'data' => [
          '#theme' => 'item_list',
          '#items' => array_column($entity->get('stripped_tags')->getValue(), 'value'),
        ],
      ],
      'stripped_attributes' => [
        'data' => [
          '#theme' => 'item_list',
          '#items' => array_column($entity->get('stripped_attributes')->getValue(), 'value'),
        ],
      ],
    ];
    if (!$content_entity) {
      return [
        'content' => $this->t('Deleted'),
        'field_name' => $entity->field_name->value,
      ] + $stripped + parent::buildRow($entity);
    }
    /** @var \Drupal\filter_format_audit\EntityHandlers\FilterFormatAuditHandlerInterface $handler */
    $handler = $this->entityTypeManager->getHandler($content_entity->getEntityTypeId(), FilterFormatAuditHandlerInterface::HANDLER_TYPE);
    $url = $handler->getUrl($content_entity);
    $label = $handler->getLabel($content_entity);
    $content = $label;
    if ($url) {
      $content = [
        'data' =>
        [
          '#type' => 'link',
          '#url' => $url,
          '#title' => $label,
        ],
      ];
    }
    $field_definition = $content_entity->getFieldDefinition($entity->field_name->value);
    $field_name = $entity->field_name->value;
    if ($field_definition) {
      $field_name = $field_definition->getLabel();
    }
    return [
      'content' => $content,
      'field_name' => $field_name,
    ] + $stripped + parent::buildRow($entity);
  }

}
