<?php

declare(strict_types=1);

namespace Drupal\filter_format_audit\Commands;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\filter_format_audit\AnalysisBatchBuilder;
use Drush\Commands\DrushCommands;

/**
 * Defines a class for drush commands for the module.
 */
final class FilterFormatAuditCommands extends DrushCommands {

  /**
   * Constructs a new FilterFormatAuditCommands.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity field manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $fieldTypePluginManager
   *   Field type plugin manager.
   */
  public function __construct(private EntityTypeManagerInterface $entityTypeManager, private EntityFieldManagerInterface $entityFieldManager, private FieldTypePluginManagerInterface $fieldTypePluginManager) {
    parent::__construct();
  }

  /**
   * Performs analysis tree.
   *
   * @usage drush filter-format-audit-analyse
   *   Regenerate analysis.
   *
   * @command filter-format-audit-analyse
   * @aliases filter-format-audit:analyse,ffaa
   */
  public function filterFormatAnalysis(): void {
    $tasks = AnalysisBatchBuilder::buildAnalysisBatch($this->entityTypeManager, $this->entityFieldManager, $this->fieldTypePluginManager);
    batch_set($tasks);
    $batch =& batch_get();
    $batch['progressive'] = FALSE;
    drush_backend_batch_process();
  }

}
