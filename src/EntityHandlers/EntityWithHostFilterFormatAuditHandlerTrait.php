<?php

namespace Drupal\filter_format_audit\EntityHandlers;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Defines a trait for entity-types that have a host entity.
 */
trait EntityWithHostFilterFormatAuditHandlerTrait {

  /**
   * Gets the host entity for an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Host entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   Host entity, if exists.
   */
  abstract protected function getHostEntity(ContentEntityInterface $entity): ?ContentEntityInterface;

  /**
   * Checks if there is a host entity for this entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to check for host.
   *
   * @return bool
   *   TRUE if there is a host entity.
   */
  abstract protected function hasHostEntity(ContentEntityInterface $entity) : bool;

  /**
   * {@inheritdoc}
   */
  public function getUrl(ContentEntityInterface $entity): ?Url {
    if (!$this->hasHostEntity($entity)) {
      return parent::getUrl($entity);
    }
    if ($dependency = $this->getHostEntity($entity)) {
      $handler = $this->entityTypeManager->getHandler($dependency->getEntityTypeId(), FilterFormatAuditHandlerInterface::HANDLER_TYPE);
      return $handler->getUrl($dependency);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(ContentEntityInterface $entity): ?string {
    /** @var \Drupal\block_content\BlockContentInterface $entity */
    if (!$this->hasHostEntity($entity)) {
      return parent::getLabel($entity);
    }
    if ($dependency = $this->getHostEntity($entity)) {
      /** @var \Drupal\filter_format_audit\EntityHandlers\FilterFormatAuditHandlerInterface $handler */
      $handler = $this->entityTypeManager->getHandler($dependency->getEntityTypeId(), FilterFormatAuditHandlerInterface::HANDLER_TYPE);
      return $handler->getLabel($dependency);
    }
    return new TranslatableMarkup('Missing host entity [@entity_type:@entity_id]', [
      '@entity_type' => $entity->getEntityTypeId(),
      '@entity_id' => $entity->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getEditUrl(ContentEntityInterface $entity): ?Url {
    /** @var \Drupal\block_content\BlockContentInterface $entity */
    if (!$this->hasHostEntity($entity)) {
      return parent::getEditUrl($entity);
    }
    if ($dependency = $this->getHostEntity($entity)) {
      /** @var \Drupal\filter_format_audit\EntityHandlers\FilterFormatAuditHandlerInterface $handler */
      $handler = $this->entityTypeManager->getHandler($dependency->getEntityTypeId(), FilterFormatAuditHandlerInterface::HANDLER_TYPE);
      return $handler->getEditUrl($dependency);
    }
    return NULL;
  }

}
