<?php

namespace Drupal\filter_format_audit;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\filter_format_audit\Entity\AnalysisResult;

/**
 * Defines a class for analysing imported content.
 */
class ContentAnalysis {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs the PathBasedBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * Analyses a node.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param string $field_name
   *   Field name.
   *
   * @return \Drupal\filter_format_audit\Entity\AnalysisResult
   *   Result.
   */
  public function analyse(ContentEntityInterface $entity, string $field_name) : AnalysisResult {
    /** @var \Drupal\filter_format_audit\Entity\AnalysisResult $result */
    $stripped_items = [
      'stripped_tags' => [],
      'stripped_attributes' => [],
    ];
    if (!$entity->hasField($field_name)) {
      AnalysisResult::create([
        'name' => $entity->label(),
        'content' => $entity,
        'field_name' => $field_name,
        'text' => $entity->get($field_name),
      ] + $stripped_items);
    }
    foreach ($entity->get($field_name) as $item) {
      $result = $this->getStrippedItems($item->value, $item->format);
      $stripped_items = [
        'stripped_tags' => array_unique(array_merge($stripped_items['stripped_tags'], $result['stripped_tags'])),
        'stripped_attributes' => array_unique(array_merge($stripped_items['stripped_attributes'], $result['stripped_attributes'])),
      ];
    }
    $result = AnalysisResult::create([
      'name' => $entity->label(),
      'content' => $entity,
      'field_name' => $field_name,
      'text' => $entity->get($field_name),
    ] + $stripped_items);
    return $result;
  }

  /**
   * Gets stripped items.
   *
   * @param string $value
   *   Field value.
   * @param string $format_id
   *   Format ID.
   *
   * @return array[]
   *   Array of stripped tags and attributes.
   */
  protected function getStrippedItems(string $value, string $format_id) : array {
    /** @var \Drupal\filter\FilterFormatInterface $filter */
    $filter = $this->entityTypeManager->getStorage('filter_format')->load($format_id);
    if (!$filter) {
      $filter = $this->entityTypeManager->getStorage('filter_format')->load($this->configFactory->get('filter.settings')->get('fallback_format'));
      if (!$filter) {
        throw new \UnexpectedValueException('The fallback filter format does not exist 🤨');
      }
    }
    /** @var \Drupal\filter\Plugin\Filter\FilterHtml $filter_html */
    $filter_html = $filter->filters('filter_html');
    if (!$filter_html || !$filter_html->status) {
      // This filter format doesn't strip HTML.
      return [
        'stripped_tags' => [],
        'stripped_attributes' => [],
      ];
    }
    $restrictions = $filter_html->getHTMLRestrictions();
    // Allow for link-it filter which may run before this filter.
    $restrictions['a'][] = 'data-entity-substitution';
    $global_allowed_attributes = array_filter($restrictions['allowed']['*']);
    unset($restrictions['allowed']['*']);
    $allowed = array_keys($restrictions['allowed']);
    $html_dom = Html::load($value);
    $xpath = new \DOMXPath($html_dom);
    $tags = [];
    $modified_attributes = [];
    foreach ($xpath->query('//*') as $element) {
      /** @var \DOMElement $element */
      if (!in_array($element->tagName, $allowed, TRUE)) {
        $tags[] = $element->tagName;
        continue;
      }
      $allowed_attributes = ['exact' => [], 'prefix' => []];
      foreach (($global_allowed_attributes + ($restrictions['allowed'][$element->tagName] ?: [])) as $name => $values) {
        // A trailing * indicates wildcard, but it must have some prefix.
        if (substr($name, -1) === '*' && $name[0] !== '*') {
          $allowed_attributes['prefix'][str_replace('*', '', $name)] = $this->prepareAttributeValues($values);
        }
        else {
          $allowed_attributes['exact'][$name] = $this->prepareAttributeValues($values);
        }
      }
      krsort($allowed_attributes['prefix']);
      foreach ($element->attributes as $name => $attribute) {
        // Remove attributes not in the list of allowed attributes.
        $allowed_value = $this->findAllowedValue($allowed_attributes, $name);
        if (empty($allowed_value)) {
          $modified_attributes[] = $element->tagName . '.' . $name;
        }
        elseif ($allowed_value !== TRUE) {
          // Check the list of allowed attribute values.
          $attribute_values = preg_split('/\s+/', $attribute->value, -1, PREG_SPLIT_NO_EMPTY);
          $removed = [];
          foreach ($attribute_values as $value) {
            if (!$this->findAllowedValue($allowed_value, $value)) {
              $removed[] = $value;
            }
          }
          if ($removed) {
            $modified_attributes[] = $element->tagName . '.' . $name . '="' . implode(' ', $removed) . '"';
          }
        }
      }

    }
    return [
      'stripped_tags' => array_diff(array_unique($tags), [
        'html',
        'head',
        'meta',
        'body',
      ]),
      'stripped_attributes' => array_unique($modified_attributes),
    ];
  }

  /**
   * Helper function to prepare attribute values including wildcards.
   *
   * Splits the values into two lists, one for values that must match exactly
   * and the other for values that are wildcard prefixes.
   *
   * @param bool|array $attribute_values
   *   TRUE, FALSE, or an array of allowed values.
   *
   * @return bool|array
   *   Values, or FALSE
   */
  protected function prepareAttributeValues($attribute_values) {
    if ($attribute_values === TRUE || $attribute_values === FALSE) {
      return $attribute_values;
    }
    $result = ['exact' => [], 'prefix' => []];
    foreach ($attribute_values as $name => $allowed) {
      // A trailing * indicates wildcard, but it must have some prefix.
      if (substr($name, -1) === '*' && $name[0] !== '*') {
        $result['prefix'][str_replace('*', '', $name)] = $allowed;
      }
      else {
        $result['exact'][$name] = $allowed;
      }
    }
    krsort($result['prefix']);
    return $result;
  }

  /**
   * Helper function to handle prefix matching.
   *
   * @param array $allowed
   *   Array of allowed names and prefixes.
   * @param string $name
   *   The name to find or match against a prefix.
   *
   * @return bool|array
   *   Allowed values or FALSE.
   */
  protected function findAllowedValue(array $allowed, $name) {
    if (isset($allowed['exact'][$name])) {
      return $allowed['exact'][$name];
    }
    // Handle prefix (wildcard) matches.
    foreach ($allowed['prefix'] as $prefix => $value) {
      if (strpos($name, $prefix) === 0) {
        return $value;
      }
    }
    return FALSE;
  }

}
