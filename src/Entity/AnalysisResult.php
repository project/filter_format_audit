<?php

namespace Drupal\filter_format_audit\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines a content entity for tracking an analysis result.
 *
 * @ContentEntityType(
 *   id = "analysis_result",
 *   label = @Translation("Analysis result"),
 *   label_collection = @Translation("Audit Results"),
 *   label_singular = @Translation("result"),
 *   label_plural = @Translation("results"),
 *   label_count = @PluralTranslation(
 *     singular = "@count result",
 *     plural = "@count results",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = \Drupal\filter_format_audit\AnalysisResultListBuilder::class,
 *     "views_data" = \Drupal\filter_format_audit\EntityHandlers\AnalysisResultViewsData::class,
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "analysis_result",
 *   admin_permission = "administer filter format audit",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "tid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/filter-format-audit/view/{analysis_result}",
 *     "collection" = "/admin/content/filter-format-audit",
 *   },
 * )
 */
class AnalysisResult extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Field name'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['content'] = BaseFieldDefinition::create('dynamic_entity_reference')
      ->setLabel(t('Analysed content'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'dynamic_entity_reference_label',
        'settings' => ['link' => TRUE],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Text content'))
      ->setRequired(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_trimmed',
        'settings' => ['trim_length' => 200],
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['stripped_tags'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Stripped tags'))
      ->setSetting('max_length', 100)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['stripped_attributes'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Stripped attributes'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    return $fields;
  }

  /**
   * Checks if there are issues.
   *
   * @return bool
   *   TRUE if there are issues.
   */
  public function hasIssues() : bool {
    return $this->get('stripped_tags')->count() + $this->get('stripped_attributes')->count() > 0;
  }

}
