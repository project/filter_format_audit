<?php

namespace Drupal\filter_format_audit\EntityHandlers;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Url;

/**
 * Defines an interface for filter format audit handler classes.
 */
interface FilterFormatAuditHandlerInterface {

  const HANDLER_TYPE = 'filter_format_audit';

  /**
   * Get the label to use for this entity.
   *
   * In the case of most entities, this will just be their label. But for e.g.
   * paragraphs or inline-blocks, it is more appropriate to use the parent/host
   * entity label.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to get the label for.
   *
   * @return string|null
   *   The label to use.
   */
  public function getLabel(ContentEntityInterface $entity): ?string;

  /**
   * Get the link to view this entity.
   *
   * In the case of most entities, this will just be their canonical link. But
   * for e.g. paragraphs or inline-blocks, it is more appropriate to use the
   * parent/host link.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to get the link for.
   *
   * @return \Drupal\Core\Url|null
   *   The url to use.
   */
  public function getUrl(ContentEntityInterface $entity): ?Url;

  /**
   * Get the edit link to use for this entity.
   *
   * In the case of most entities, this will just be their edit form. But for
   * e.g. paragraphs or inline-blocks, it is more appropriate to use the parent/
   * host entity edit form.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to get the label for.
   *
   * @return \Drupal\Core\Url|null
   *   The edit link URL.
   */
  public function getEditUrl(ContentEntityInterface $entity): ?Url;

}
