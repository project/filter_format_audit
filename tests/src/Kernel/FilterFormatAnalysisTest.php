<?php

namespace Drupal\Tests\filter_format_audit\Kernel;

use Drupal\Core\Serialization\Yaml;
use Drupal\filter\Entity\FilterFormat;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Defines a test for MigratedContentAnalysis.
 *
 * @group filter_format_audit
 */
class FilterFormatAnalysisTest extends KernelTestBase {

  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'options',
    'field',
    'path_alias',
    'path',
    'text',
    'system',
    'dynamic_entity_reference',
    'filter_format_audit',
    'user',
    'filter',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('analysis_result');
    $this->installEntitySchema('user');
    $this->installEntitySchema('path_alias');
    $this->installEntitySchema('node');
    $this->installConfig(['system', 'node', 'filter']);
    $filter_config = <<<YML
filter_html:
    id: filter_html
    provider: filter
    status: true
    weight: -50
    settings:
      allowed_html: '<em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd> <h2> <h3 class="allowed-class"> <h4> <h5> <h6> <p class="text--center"> <br> <span> <img src data-entity-type data-entity-uuid data-align data-caption alt> <a target href data-entity-type data-entity-uuid title class="button"> <sup id> <sub> <hr> <table> <caption> <tbody> <thead> <tfoot> <th> <td> <tr> <div><drupal-media data-entity-type data-entity-uuid data-entity-substitution data-view-mode data-align data-caption alt title>'
      filter_html_help: false
      filter_html_nofollow: false
YML;
    FilterFormat::create([
      'name' => 'Basic HTML',
      'format' => 'basic_html',
      'filters' => Yaml::decode($filter_config),
    ])->save();
    $this->createContentType(['type' => 'article']);
  }

  /**
   * Tests broken link analysis.
   */
  public function testAnalysis() {
    $analysis = \Drupal::service('filter_format_audit.content_analysis');
    $node = $this->createNode([
      'type' => 'article',
      'body' => [
        'value' => '<script>Yep</script><h3 class="allowed-class">Hi</h3><span class="whoops">Ok</span>',
        'format' => 'basic_html',
      ],
    ]);
    $result = $analysis->analyse($node, 'body');
    $this->assertTrue($result->hasIssues());
    $this->assertEquals($node->id(), $result->content->target_id);
    $this->assertEquals('body', $result->field_name->value);
    $this->assertEquals('node', $result->content->target_type);
    $this->assertContains('script', array_column($result->get('stripped_tags')->getValue(), 'value'));
    $stripped_attributes = array_column($result->get('stripped_attributes')->getValue(), 'value');
    $this->assertContains('span.class', $stripped_attributes);
    $this->assertNotContains('h3.class', $stripped_attributes);
  }

}
