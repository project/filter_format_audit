<?php

namespace Drupal\filter_format_audit\EntityHandlers;

use Drupal\block_content\Access\DependentAccessInterface;
use Drupal\block_content\BlockContentEvents;
use Drupal\block_content\Event\BlockContentGetDependencyEvent;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a filter format audit handler for block content entities.
 */
class BlockContentFilterFormatAuditHandler extends FilterFormatAuditHandlerDefault implements EntityHandlerInterface {

  use EntityWithHostFilterFormatAuditHandlerTrait;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function hasHostEntity(ContentEntityInterface $entity): bool {
    /** @var \Drupal\block_content\BlockContentInterface $entity */
    return !$entity->isReusable();
  }

  /**
   * Gets the host entity for an inline block.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Host entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   Host entity, if exists.
   */
  protected function getHostEntity(ContentEntityInterface $entity) : ?ContentEntityInterface {
    /** @var \Drupal\block_content\BlockContentInterface $entity */
    if (!$entity instanceof DependentAccessInterface) {
      return NULL;
    }
    $dependency = $entity->getAccessDependency();
    try {
      if (empty($dependency)) {
        // If an access dependency has not been set let modules set one.
        $event = new BlockContentGetDependencyEvent($entity);
        $this->eventDispatcher->dispatch($event, BlockContentEvents::BLOCK_CONTENT_GET_DEPENDENCY);
        $dependency = $event->getAccessDependency();
      }
    }
    catch (\Throwable $e) {
      // If the dependent entity has been deleted, this can occur.
      return NULL;
    }
    return $dependency instanceof ContentEntityInterface ? $dependency : NULL;
  }

}
